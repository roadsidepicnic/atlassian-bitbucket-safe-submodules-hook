Safe Submodules Hook for Atlassian Bitbucket Server
===================================================

This Hook rejects commits with invalid submodules references.
 
Usage
-----

You should enable hook in repo hooks list only. No settings needed.
  
When you pushing commits which referencing to nonexistent commit of submodule you will see message like this:

![Safe Submodules Hook](./safe-submodules-hook.png)