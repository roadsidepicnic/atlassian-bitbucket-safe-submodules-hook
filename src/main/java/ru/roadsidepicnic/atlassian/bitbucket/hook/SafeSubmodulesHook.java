package ru.roadsidepicnic.atlassian.bitbucket.hook;

import ca.szc.configparser.Ini;
import ca.szc.configparser.exceptions.IniParserException;
import com.atlassian.bitbucket.NoSuchEntityException;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.content.*;
import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.io.TypeAwareOutputSupplier;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.RefChangeType;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import org.apache.commons.collections.map.DefaultedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;


public class SafeSubmodulesHook implements PreReceiveRepositoryHook {
    private static final Logger log = LogManager.getLogger(SafeSubmodulesHook.class);

    private final RepositoryService repositoryService;
    private final CommitService commitService;
    private final ContentService contentService;
    private final NavBuilder navBuilder;

    // @Autowired
    public SafeSubmodulesHook(
            @Nonnull RepositoryService repositoryService,
            @Nonnull CommitService commitService,
            @Nonnull ContentService contentService,
            @Nonnull NavBuilder navBuilder
    ) {
        this.repositoryService = repositoryService;
        this.commitService = commitService;
        this.contentService = contentService;
        this.navBuilder = navBuilder;
    }

    @Override
    public boolean onReceive(@Nonnull RepositoryHookContext context,
                             @Nonnull Collection<RefChange> refChanges,
                             @Nonnull HookResponse hookResponse) {

        List<Change> invalidSubmoduleRefChanges = getInvalidSubmoduleRefChanges(context, refChanges);
        if (!invalidSubmoduleRefChanges.isEmpty()) {
            hookResponse.err().println(header());
            hookResponse.err().println("The commits you are pushing is referencing nonexistent commits");
            hookResponse.err().println("of the following submodules:");
            for (Change change : invalidSubmoduleRefChanges) {
                hookResponse.err().println("\t" + change.getPath() + " @ " + change.getContentId());
            }
            hookResponse.err().println("");
            hookResponse.err().println("Perhaps you or someone else forgot to push submodules.");
            hookResponse.err().println(footer());
            return false;
        }

        return true;
    }

    private List<Change> getInvalidSubmoduleRefChanges(RepositoryHookContext repositoryHookContext, Collection<RefChange> refChanges) {
        List<Change> invalidSubmoduleRefChanges = new ArrayList<>();
        for (RefChange refChange : refChanges) {
            // Ignore deleted refs
            if (refChange.getType() == RefChangeType.DELETE) {
                continue;
            }
            // Is it faster to do walk the file tree first? How often does a repository have a .gitmodules file? Questions...
            final Map<String, URI> submodules = getAvailableSubmodules(repositoryHookContext, refChange);
            if (submodules.isEmpty()) {
                log.debug(String.format(
                        "There is no submodules defined in commit %s of repository %s/%s",
                        refChange.getToHash(),
                        repositoryHookContext.getRepository().getProject().getKey(),
                        repositoryHookContext.getRepository().getSlug()
                ));
                continue;
            }

            for (Change change : getChangedSubmodules(refChange, repositoryHookContext.getRepository())) {
                URI submoduleUrl = submodules.get(change.getPath().toString());

                if (submoduleUrl == null) {
                    continue;
                }

                submoduleUrl = resolveRelativePath(submoduleUrl, repositoryHookContext.getRepository());
                String[] slug = extractRepoSlug(submoduleUrl);
                if (slug == null) {
                    log.debug(String.format("Can not extract slug from url '%s'", submoduleUrl));
                    continue;
                }

                Repository repo = repositoryService.getBySlug(slug[0], slug[1]);
                if (repo == null) {
                    log.debug(String.format("Can not find repository by slug '%s'", Arrays.toString(slug)));
                    continue;
                }

                if (commitExistsInRepo(repo, change.getContentId())) {
                    continue;
                }

                invalidSubmoduleRefChanges.add(change);
            }
        }

        return invalidSubmoduleRefChanges;
    }

    private Map<String, URI> getAvailableSubmodules(RepositoryHookContext repositoryHookContext, RefChange refChange) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            contentService.streamFile(repositoryHookContext.getRepository(), refChange.getToHash(), ".gitmodules", new TypeAwareOutputSupplier() {
                @Nonnull
                @Override
                public OutputStream getStream(@Nonnull String contentType) throws IOException {
                    return out;
                }
            });
            Map<String, URI> submodules = new HashMap<>();

            Ini gitmodules = new Ini().read(new BufferedReader(new StringReader(out.toString("UTF-8"))));

            for (Map.Entry<String, Map<String, String>> section : gitmodules.getSections().entrySet()) {
                Map<String, String> params = DefaultedMap.decorate(section.getValue(), "");

                URI submoduleUrl = URI.create(params.get("url"));

                if (!isOnCurrentServer(submoduleUrl)) {
                    log.debug(String.format("Submodule %s (%s) not from same server. Skipping.", params.get("path"), params.get("url")));
                    continue;
                }

                submodules.put(params.get("path"), submoduleUrl);
            }

            return submodules;
        } catch (NoSuchPathException e) {
            return Collections.emptyMap();
        } catch (IniParserException e) {
            return Collections.emptyMap();
        } catch (IOException e) {
            return Collections.emptyMap();
        }
    }

    private List<Change> getChangedSubmodules(RefChange refChange, Repository repository) {
        final List<Change> changes = new ArrayList<>();

        ChangesRequest.Builder request = new ChangesRequest.Builder(repository, refChange.getToHash());
        if (refChange.getType() == RefChangeType.UPDATE) {
            request = request.sinceId(refChange.getFromHash());
        }
        commitService.streamChanges(request.build(), new AbstractChangeCallback() {
            @Override
            public boolean onChange(@Nonnull Change change) throws IOException {
                if (change.getNodeType() == ContentTreeNode.Type.SUBMODULE) {
                    changes.add(change);
                }
                // Keep streaming until the end
                return true;
            }
        });
        return changes;
    }

    private URI resolveRelativePath(URI uri, Repository repository) {
        String url = uri.toASCIIString();
        if (url.startsWith("../") || url.startsWith("./")) {
            try {
                String httpUrl = navBuilder.repo(repository).buildAbsolute();
                // Note: The / at the end of the uri is required for the resolve to work correctly
                return new URI(httpUrl + "/").resolve(uri);
            } catch (URISyntaxException e) {
                // Never happen unless RepositoryService is sick
            }
        }
        return uri;
    }

    private String[] extractRepoSlug(URI repoUrl) {
        String path = repoUrl.getPath().replaceAll("^/(.*?)(?:\\.git)?$", "$1");
        if (repoUrl.getScheme().startsWith("http")) {
            path = path.replaceAll("^(stash|bitbucket)/scm/", "");
        }
        String[] parts = path.split("/");

        if (parts.length == 2) { // First part is always blank due to leading slash
            return parts;
        }
        return null;
    }

    private boolean commitExistsInRepo(Repository repo, String commit) {
        try {
            commitService.getCommit(new CommitRequest.Builder(repo, commit).build());
            return true;
        } catch (NoSuchEntityException e) {
            return false;
        }
    }

    private boolean isOnCurrentServer(URI uri) {
        URI currentServerUrl = URI.create(navBuilder.buildAbsolute());

        return currentServerUrl.getHost().equals(uri.getHost()) && (
                (uri.getPort() == currentServerUrl.getPort() && uri.getPath().startsWith(currentServerUrl.getPath())) ||
                        uri.getScheme().equals("ssh")
        );
    }

    private static String header() {
        return StringUtils.rightPad("", 100, "=") + "\n" +
            " __        __        __       _                         _       _                               _    \n" +
            "/ _\\ __ _ / _| ___  / _\\_   _| |__  _ __ ___   ___   __| |_   _| | ___  ___    /\\  /\\___   ___ | | __\n" +
            "\\ \\ / _` | |_ / _ \\ \\ \\| | | | '_ \\| '_ ` _ \\ / _ \\ / _` | | | | |/ _ \\/ __|  / /_/ / _ \\ / _ \\| |/ /\n" +
            "_\\ \\ (_| |  _|  __/ _\\ \\ |_| | |_) | | | | | | (_) | (_| | |_| | |  __/\\__ \\ / __  / (_) | (_) |   < \n" +
            "\\__/\\__,_|_|  \\___| \\__/\\__,_|_.__/|_| |_| |_|\\___/ \\__,_|\\__,_|_|\\___||___/ \\/ /_/ \\___/ \\___/|_|\\_\\\n" +
            "                                                                                                     " +
            StringUtils.leftPad("by Roadside Picnic (http://roadsidepicnic.ru)", 100, " ") + "\n"
        ;
    }

    private static String footer() {
        return StringUtils.rightPad("", 100, "=") + "\n";
    }
}
